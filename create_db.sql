drop table if exists HAS;
drop table if exists FEATURING;
drop table if exists TRACK;
drop table if exists ALBUM;
drop table if exists GENRE;
drop table if exists ARTIST;
drop table if exists USER;

create table USER(
  idUs int primary key auto_increment,
  nameUs varchar(15) unique,
  mailUs varchar(255) unique,
  passUs char(64),
  iconUs blob
);

create table ARTIST(
  idAr int primary key auto_increment,
  nameAr varchar(30),
  imageAr blob
);

create table GENRE(
  idGe int primary key auto_increment,
  nameGe varchar(30)
);

create table ALBUM(
  idAl int primary key auto_increment,
  titleAl varchar(30),
  yearAl year,
  coverAl blob
);

create table TRACK(
  idTr int primary key auto_increment,
  titleTr varchar(30),
  lengthTr smallint unsigned,
  idGe int,
  idAl int,
  posTr tinyint unsigned,
  ytTr varchar(15),

  foreign key (idAl) references ALBUM(idAl),
  foreign key (idGe) references GENRE(idGe)
);

create table FEATURING(
  idTr int,
  idAr int,

  foreign key (idTr) references TRACK(idTr),
  foreign key (idAr) references ARTIST(idAr),
  primary key (idTr, idAr)
);

create table HAS(
  idUs int,
  idAl int,
  mark tinyint,

  foreign key (idUs) references USER(idUs),
  foreign key (idAl) references ALBUM(idAl),
  primary key (idUs, idAl)
);
