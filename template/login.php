<?php
  session_start();
  // Connection BD
  $bd = mysqli_connect("servinfo-mariaDB", "sleroy", "sleroy", "DBsleroy");
  $_SESSION['bd'] = mysqli_connect("servinfo-mariaDB", "sleroy", "sleroy", "DBsleroy");
  if ($bd->connect_error) {
    die("Connection failed: " . $bd->connect_error);
  }
  // echo "Connected successfully";


   if(isset($_POST['valider'])){
     $username = mysqli_real_escape_string($bd, $_POST['ndcompte']);
     $email = mysqli_real_escape_string($bd, $_POST['email']);
     $password = mysqli_real_escape_string($bd, $_POST['password']);
     $password2 = mysqli_real_escape_string($bd, $_POST['password2']);

     $sql = "SELECT * from USER where nameUs='$username' or mailUs='$email'";
     $query = mysqli_query($bd, $sql);
     $count = mysqli_num_rows($query);

     if($count == 0){
       if($password == $password2){
         // Creation user
         $hashword = hash("sha256", hash("sha256", $username).hash("sha256", $password));
         $sql = "INSERT INTO USER(nameUs, mailUs, passUs) VALUES('$username', '$email', '$hashword')";
         mysqli_query($bd, $sql);
         $_SESSION['inscr_message'] = "Vous êtes maintenant connecté";
         $_SESSION['username'] = $username;
         header("location: accueil.php"); // redirection
       }
       else{
         // mdp pas correspondants
         $_SESSION['inscr_message'] = "Les deux mots de passe ne correspondent pas";
       }
     }
     else{
       $_SESSION['inscr_message'] = "Pseudo ou mail déjà utilisé";
     }
   }

   if(isset($_POST['login'])){
     $log_username = mysqli_real_escape_string($bd, $_POST['login_ndcompte']);
     $log_password = mysqli_real_escape_string($bd, $_POST['login_password']);

     $log_hashword = hash("sha256", hash("sha256", $log_username).hash("sha256", $log_password));

     $query = $bd->prepare("SELECT * FROM USER where nameUs='$log_username' AND passUs='$log_hashword'");
     $query->execute();
     $query->store_result();
     $rows = $query->num_rows;

     if ($rows == 1){
       $_SESSION['log_message'] = "Vous êtes maintenant connecté";
       $_SESSION['username'] = $log_username;
       header("location: accueil.php"); // redirection
     }
     else{
       $_SESSION['log_message'] = "Utilisateur ou mot de passe incorect";
     }
   }
 ?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../static/login.css"/>
    <title>MyAlbumList - Connexion</title>
  </head>
  <header>
    <h1>MyAlbumList</h1>
  </header>
  <body>
    <div id="sepG">
    </div>
    <section id="Inscription">
    <form class="Inscription" action="login.php" method="post">
      <h2>Inscription</h2>
      <table>
        <tr>
          <td>Nom de compte</td>
          <td><input required type="text" name="ndcompte" size="25" maxlength="15"
          pattern="[az-Az-Za]{1,15}"
          title="Nom de compte sans caractère speciaux ( -, ; / .. ), ni chiffre de 15 caractère maximum."
          placeholder=""></td>
        </tr>
        <tr>
          <td>Mail</td>
          <td><input required type="email" name="email" size="25"></td>
        </tr>
        <tr>
          <td>Mot de passe</td>
          <td><input required type="password" name="password" size="25" maxlength="15"
          pattern="[az-Az-Za-0-9]{1,15}"
          title="Mot de passe sans caractère speciaux ( -, ; / .. ) de 15 caractère maximum."></td>
        </tr>
        <tr>
          <td>Confirmation de mot de passe</td>
          <td><input required type="password" name="password2" size="25" maxlength="15"
          pattern="[az-Az-Za-0-9]{1,15}"
          title="Mot de passe sans caractère speciaux ( -, ; / .. ) de 15 caractère maximum."></td>
        </tr>
        <?php if (isset($_SESSION['inscr_message'])) {
          echo "<tr><td id='erreur'>".$_SESSION['inscr_message']."</tr></td>";
        } ?>
      <tr>
        <td>
          <input type="submit" name="valider" value="Creer le compte">
        </td>
      </tr>
    </table>
    </form>
  </section>

  <section id="Connexion">
  <form class="Connexion" action="login.php" method="post">
    <h2>Connexion</h2>
    <table>
      <tr>
        <td>Nom de compte</td>
        <td> <input required type="text" name="login_ndcompte" size="25" maxlength="20"
        pattern="[az-Az-Za]{1,15}"
        title="Nom de compte sans caractère speciaux ( -, ; / .. ), ni chiffre de 15 caractère maximum."
        placeholder=""></td>
      </tr>

    <tr>
      <td>Mot de passe</td>
      <td><input required type="password" name="login_password" size="25" maxlength="15"
      pattern="[az-Az-Za-0-9]{1,15}"
      title="Mot de passe sans caractère speciaux ( -, ; / .. ) de 15 caractère maximum."></td>
    </tr>
    <?php if (isset($_SESSION['log_message'])) {
      echo "<tr><td>".$_SESSION['log_message']."</tr></td>";
    } ?>
    <tr>
      <td>
        <input type="submit" name="login" value="Se connecter">
      </td>
    </tr>
  </table>
  </form>
</section>
<div id="sepD">
</div>
<div id="sepG2">
</div>
<section id="Resume">
  <h2>MyAlbumList, c'est quoi ?</h2>
  <p>
    MyAlbumList (MAL) est un site qui permet à ses utilisateurs de garder un oeil sur leurs albums et artiste préférés.
    Inscris toi à MAL et créer ta bibliothèque musicale !
  </p>
</section>
<div id="pushFooter">
</div>
<footer>
  <section>

    <h2>Information</h2>
    <p>Site créé par Sosthène Leroy, Erik Vienne et Thomas Quetier dans le cadre d'un projet d'étude</p>
    <p>Pour tout contact = <a href="mailto:sosthene.leroy@etu.univ-orleans.fr">sosthene.leroy@etu.univ-orleans.fr</a></p>

  </section>

  <img src="../resources/logoIUT.png" alt="IUT orleans" height="100" width="150">
</footer>
  </body>
</html>
