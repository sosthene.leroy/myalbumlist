<?php
  session_start();
  $bd = mysqli_connect("servinfo-mariaDB", "sleroy", "sleroy", "DBsleroy");
 ?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <link rel="stylesheet" href="../static/accueil.css"/>
    <meta charset="utf-8">
    <title>Bienvenue sur MAL</title>
  </head>
  <body>
    <header>
      <h2><a href="accueil.php">MyAlbumList</a></h2>
      <img id="search" src="../resources/search.png" alt="search logo" height="28" width="28">

      <form class="Recherche" action="recherche.php" method="post">
        <p>
          <input name="search" type="text" pattern="[A-Za-z- ]{1,}"
          title="Entrer un mot clé de recherche" maxlength="100"
          placeholder="Nom d'un artiste, d'un album,...">
          <input type="submit" name="rechercher" value="Rechercher">
        </p>
      </form>

      <?php if(isset($_SESSION['username'])){?>
      <img id="profile" src="../resources/profile.png" alt="search logo" height="28" width="28">
      <h1 id="nomUser"><?php echo $_SESSION['username'];?></h1>
      <a id="deco" href="logout.php">Se déconnecter</a>
      <?php }
        else{?>
          <a id="deco" href="login.php">Se connecter</a>
        <?php }
       ?>
    </header>
    <div>
    </div>
    <section id="navP">
      <form class="" action="liste.php" method="post">
        <table>
          <tr>
            <th><input type="submit"  name="albums" value="Albums" /></th>
            <th><input type="submit"  name="artistes" value="Artistes" /></th>
            <th><input type="submit"  name="titres" value="Titres" /></th>
            <th><input type="submit"  name="genres" value="Genres" /></th>
          </tr>
        </table>
      </form>
    </section>
    <div>
    </div>
    <?php
    if(isset($_SESSION['username'])){
      $username = $_SESSION['username'];
      $sql_id = "SELECT * from USER where nameUs = '$username'";
      $query_id = mysqli_query($bd, $sql_id);
      while ($row = mysqli_fetch_array($query_id)) {
        $id_us = $row['idUs'];
      }
      $sql_has = "SELECT * from HAS where idUs = '$id_us'";
      $query_has = mysqli_query($bd, $sql_has);
      $count = mysqli_num_rows($query_has);


      if($count == 0){
        echo 'Vous n\'avez pas encore ajouté d\'albums...';
      }
      else {
        echo "Vous avez : <br>";
        ?>
        <form class="" action="info.php" method="post">
          <?php
          while ($row_has = mysqli_fetch_array($query_has)) {
            $id_al = $row_has['idAl'];
            $sql_album = "SELECT * from ALBUM where idAl = '$id_al'";
            $query_album = mysqli_query($bd, $sql_album);
            $mark = $row_has['mark'];
            while ($row_album = mysqli_fetch_array($query_album)) {
              $nom_album = $row_album['titleAl'];
              echo "<input type='submit' name='album' value='$nom_album'>, noté $mark sur 5 <br>";
            }
          }
           ?>
        </form>
        <?php
      }
    }
    else{
      echo "<a href='login.php'>Connectez-vous pour voir et ajouter vos albums</a>";
    }
     ?>
    <div id="pushFooter">
    </div>

    <footer>
  <section>

  <h2>Information</h2>
  <p>Site créé par Sosthène Leroy, Erik Vienne et Thomas Quetier dans le cadre d'un projet d'étude</p>
  <p>Pour tout contact = <a href="mailto:sosthene.leroy@etu.univ-orleans.fr">sosthene.leroy@etu.univ-orleans.fr</a></p>

</section>

 <img src="../resources/logoIUT.png" alt="IUT orleans" height="100" width="150">
</footer>

  </body>
</html>
