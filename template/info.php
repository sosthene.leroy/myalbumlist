<?php
session_start();
$bd = mysqli_connect("servinfo-mariaDB", "sleroy", "sleroy", "DBsleroy");
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <link rel="stylesheet" href="../static/info.css"/>
  <meta charset="utf-8">
  <title>Informations</title>
</head>
<body>
  <header>
    <h2><a href="accueil.php">MyAlbumList</a></h2>
    <img id="search" src="../resources/search.png" alt="search logo" height="28" width="28">

    <form class="Recherche" action="recherche.php" method="post">
      <p>
        <input name="search" type="text" pattern="[A-Za-z- ]{1,}"
        title="Entrer un mot clé de recherche" maxlength="100"
        placeholder="Nom d'un artiste, d'un album,...">
        <input type="submit" name="rechercher" value="Rechercher">
      </p>
    </form>

    <?php if(isset($_SESSION['username'])){?>
      <img id="profile" src="../resources/profile.png" alt="search logo" height="28" width="28">
      <h1 id="nomUser"><?php echo $_SESSION['username'];?></h1>
      <a id="deco" href="logout.php">Se déconnecter</a>
    <?php }
    else{?>
      <a id="deco" href="login.php">Se connecter</a>
    <?php }
    ?>
  </header>
  <div id="gauche">

  </div>
  <section id= "info">
    <?php
    if(isset($_POST['album'])){
      $nomAl = $_POST['album'];
      echo "<h2> $nomAl </h2>";

      $genres = "<h2> Genres de l'album : ";
      $sql_genres = "select distinct nameGe from GENRE natural join TRACK natural join ALBUM where titleAl = '$nomAl'";
      $query = mysqli_query($bd, $sql_genres);
      while ($row = mysqli_fetch_array($query)) {
        $res = $row['nameGe'];
        $genres .= $res;
      }
      $genres .= "</h2>";
      echo $genres;

      $sql_idAl = "select idAl from ALBUM where titleAl = '$nomAl'";
      $query = mysqli_query($bd, $sql_idAl);
      $row = mysqli_fetch_row($query);
      $id_al = $row[0];
      $_SESSION['idAl'] = $id_al;

      $sql_artistes = "select distinct nameAr from ARTIST natural join FEATURING natural join TRACK natural join ALBUM where idAl = '$id_al'";
      $query = mysqli_query($bd, $sql_artistes);
      echo "Par ";
      ?>
      <form class="" action="info.php" method="post">
<?php
      while ($row = mysqli_fetch_array($query)) {
        $res = $row['nameAr'];
        echo "<input type='submit' name='artiste' value='$res'>";
      }
      ?>
    </form>
      <?php
      if(isset($_SESSION['username'])){
        $username = $_SESSION['username'];
        $sql_id = "SELECT * from USER where nameUs = '$username'";
        $query_id = mysqli_query($bd, $sql_id);
        while ($row = mysqli_fetch_array($query_id)) {
          $id_us = $row['idUs'];
        }
        $sql_has = "select * from HAS where idUs = '$id_us' and idAl = '$id_al'";
        $query_has = mysqli_query($bd, $sql_has);
        $count = mysqli_num_rows($query_has);
        if($count == 0){ ?>
          <p>
            Donner une note a l'album :
            <form class="" action="ajout_album.php" method="post">
              0<input type="radio" name="note" value="0" checked="checked">
              1<input type="radio" name="note" value="1">
              2<input type="radio" name="note" value="2">
              3<input type="radio" name="note" value="3">
              4<input type="radio" name="note" value="4">
              5<input type="radio" name="note" value="5">
              <input id="valnote" type="submit" name="valNote" value="Ajouter l'album à votre liste">
            </form>

          </p>
          <?php
        }
        else {
          ?>
          <p>
            Donner une note a l'album :
            <form class="" action="modif_note.php" method="post">
              0<input type="radio" name="note" value="0" checked="checked">
              1<input type="radio" name="note" value="1">
              2<input type="radio" name="note" value="2">
              3<input type="radio" name="note" value="3">
              4<input type="radio" name="note" value="4">
              5<input type="radio" name="note" value="5">
              <input id="valnote" type="submit" name="valNote" value="Modifier la note">
            </form>
          </p>
          <?php
        }
      }
      ?>
      <h3>Morceaux</h3>
      <?php
      $sql_morceaux = "select * from ARTIST natural join FEATURING natural join TRACK natural join ALBUM where idAl = '$id_al' group by posTr";
      $query = mysqli_query($bd, $sql_morceaux);
      ?>
      <form class="" action="info.php" method="post">
      <?php
      while ($row = mysqli_fetch_array($query)) {
        $res = $row['titleTr'];
        $res_num = $row['posTr'];
        $res_nomAl = $row['titleAl'];
        echo "$res_num <input type='submit' name='track' value='$res ($res_nomAl)'> <br>";
      }
      ?>
      </form>
      <?php
    }
    if(isset($_POST['artiste'])){
      $nomAr = $_POST['artiste'];
      echo "<h1>Albums de $nomAr :</h1>";
      $sql_idAr = "select idAr from ARTIST where nameAr = '$nomAr'";
      $query = mysqli_query($bd, $sql_idAr);
      $row = mysqli_fetch_row($query);
      $id_ar = $row[0];
      $sql_albums = "select distinct titleAl from ARTIST natural join FEATURING natural join TRACK natural join ALBUM where idAr = '$id_ar' order by titleAl";
      $query = mysqli_query($bd, $sql_albums);
      $count = mysqli_num_rows($query);
      if($count == 0){
        echo "Pas d'album enregistré pour cet artiste :(";
      }
      else{
      ?>
      <form class="" action="info.php" method="post">
        <?php
          while ($row = mysqli_fetch_array($query)) {
            $res = $row['titleAl'];
            echo "<input type='submit' name='album' value='$res'> <br>";
          }
        ?>
      </form>
      <?php
    }
  }
  if(isset($_POST['track'])){
    $data = $_POST['track'];
    $nomTr = substr($data, 0, strpos($data, "("));
    $nomAl = substr($data, strpos($data, "(") + 1, -1);
    $sql_ytb = "select ytTr from TRACK natural join ALBUM where titleTr = '$nomTr' and titleAl = '$nomAl'";
    $query = mysqli_query($bd, $sql_ytb);
    $row = mysqli_fetch_row($query);
    $tag_ytb = $row[0];
    echo "<h1> $nomTr</h1>";
    ?>
    <form class="" action="info.php" method="post">
      <?php
          echo "<h2>De l'album <input type='submit' name='album' value='$nomAl'> </h2> <br>";
      ?>
    </form>
    <?php
    echo "<iframe width='420' height='315' src='https://www.youtube.com/embed/$tag_ytb'> </iframe>";
}
    ?>
    <p><a id ="retour" href="accueil.php">Retour</a></p>

  </section>
  <div id="droite">

  </div>
  <div id="pushFooter">
  </div>
  <footer>
    <section>

      <h2>Information</h2>
      <p>Site créer par Sothène Leroy, Erik Vienne et Thomas Quetier dans le cadre d'un projet d'étude</p>
      <p>Pour tout contact = <a href="mailto:sosthene.leroy@etu.univ-orleans.fr">sothene.leroy@etu.univ-orleans.fr</a></p>

    </section>

    <img src="../resources/logoIUT.png" alt="IUT orleans" height="100" width="150">
  </footer>
</body>
</html>
