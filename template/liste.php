<?php
session_start();
$bd = mysqli_connect("servinfo-mariaDB", "sleroy", "sleroy", "DBsleroy");
$output ='';
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>MyAlbumList</title>
  <link rel="stylesheet" href="../static/liste.css"/>
</head>
<body>
  <header>
    <h2><a href="accueil.php">MyAlbumList</a></h2>
    <img id="search" src="../resources/search.png" alt="search logo" height="28" width="28">

    <form class="Recherche" action="recherche.php" method="post">
      <p>
        <input name="search" type="text" pattern="[A-Za-z- ]{1,}"
        title="Entrer un mot clé de recherche" maxlength="100"
        placeholder="Nom d'un artiste, d'un album,...">
        <input type="submit" name="rechercher" value="Rechercher">
      </p>
    </form>

    <?php if(isset($_SESSION['username'])){?>
      <img id="profile" src="../resources/profile.png" alt="search logo" height="28" width="28">
      <h1 id="nomUser"><?php echo $_SESSION['username'];?></h1>
      <a id="deco" href="logout.php">Se déconnecter</a>
    <?php }
    else{?>
      <a id="deco" href="login.php">Se connecter</a>
    <?php }
    ?>
  </header>
  <div>

  </div>
  <section class="afficheListe">
    <?php
    if(isset($_POST['albums'])){
      echo "<h2>Voici les Albums :</h2>";
      $sql = "SELECT * from ALBUM";
      $query = mysqli_query($bd, $sql);
      ?>
      <form class="" action="info.php" method="post">
        <?php
        while ($row = mysqli_fetch_array($query)) {
          $res = $row['titleAl'];
          echo "<input type='submit' name='album' value='$res'> <br>";
        }
        ?>
      </form>
      <?php

    }
    if(isset($_POST['artistes'])){
      echo "<h2>Voici les Artistes :</h2>";
      $sql = "SELECT * from ARTIST";
      $query = mysqli_query($bd, $sql);
      ?>
      <form class="" action="info.php" method="post">
        <?php
        while ($row = mysqli_fetch_array($query)) {
          $res = $row['nameAr'];
          echo "<input type='submit' name='artiste' value='$res'> <br>";
        }
        ?>
      </form>
      <?php
    }
    if(isset($_POST['titres'])){
      echo "<h2>Voici les Titres :</h2>";
      $sql = "SELECT * from TRACK natural join ALBUM";
      $query = mysqli_query($bd, $sql);
      ?>
      <form class="" action="info.php" method="post">
        <?php
        while ($row = mysqli_fetch_array($query)) {
          $res_morceau = $row['titleTr'];
          $res_morceau_album = $row['titleAl'];
          echo "<input type='submit' name='track' value='$res_morceau ($res_morceau_album)'> <br>";
        }
        ?>
      </form>
      <?php
    }
    if(isset($_POST['genres'])){
      $output .= "<h2>Voici les Genres :</h2>";
      $sql = "SELECT * from GENRE";
      $query = mysqli_query($bd, $sql);
      while ($row = mysqli_fetch_array($query)) {
        $res = $row['nameGe'];
        $output .= '<p>'.$res.'</p>';
      }
    }

    echo $output;
    ?>


  </section>
  <div id="pushFooter">
  </div>

  <footer>
    <section>

      <h2>Information</h2>
      <p>Site créé par Sosthène Leroy, Erik Vienne et Thomas Quetier dans le cadre d'un projet d'étude</p>
      <p>Pour tout contact = <a href="mailto:sosthene.leroy@etu.univ-orleans.fr">sosthene.leroy@etu.univ-orleans.fr</a></p>

    </section>

    <img src="../resources/logoIUT.png" alt="IUT orleans" height="100" width="150">
  </footer>
</body>
</html>
