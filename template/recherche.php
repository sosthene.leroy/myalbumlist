<?php
  session_start();
  $bd = mysqli_connect("servinfo-mariaDB", "sleroy", "sleroy", "DBsleroy");
  $output ='';
     ?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <link rel="stylesheet" href="../static/resultat.css"/>
    <meta charset="utf-8">
    <title>Resultat recherche</title>
  </head>
  <body>
    <header>
      <h2><a href="accueil.php">MyAlbumList</a></h2>
      <img id="search" src="../resources/search.png" alt="search logo" height="28" width="28">

      <form class="Recherche" action="recherche.php" method="post">
        <p>
          <input name="search" type="text" pattern="[A-Za-z- ]{1,}"
          title="Entrer un mot clé de recherche" maxlength="100"
          placeholder="Nom d'un artiste, d'un album,...">
          <input type="submit" name="rechercher" value="Rechercher">
        </p>
      </form>
      <?php if(isset($_SESSION['username'])){?>
      <img id="profile" src="../resources/profile.png" alt="search logo" height="28" width="28">
      <h1><?php echo $_SESSION['username'];?></h1>
      <a id = "deco" href="logout.php">Se déconnecter</a>
      <?php }
      else{?>
        <a id="deco" href="login.php">Se connecter</a>
      <?php }
     ?>
    </header>
    <?php
    if(isset($_POST['rechercher'])){
      $keyword = $_POST['search'];

      $sql = "SELECT * from ARTIST where nameAr like '%$keyword%'";
      $sql2 = "SELECT * from ALBUM where titleAl like '%$keyword%'";
      $sql3 = "SELECT * from TRACK natural join ALBUM where titleTr like '%$keyword%'";

      $query = mysqli_query($bd, $sql);
      $query2 = mysqli_query($bd, $sql2);
      $query3 = mysqli_query($bd, $sql3);

      $count = mysqli_num_rows($query);
      $count2 = mysqli_num_rows($query2);
      $count3 = mysqli_num_rows($query3);
      if($count == 0 && $count2 == 0 && $count3 == 0){
        $output = "Aucun resultat ne correspond à votre recherche";
      }
      else{
        ?>
        <section id="corps">
        <form class="" action="info.php" method="post">
          <!-- <div id="gauche"></div> -->
        <?php
        echo "<section id='artiste'>";
        echo "Artiste(s) :<br>";
        while ($row = mysqli_fetch_array($query)) {
          $res = $row['nameAr'];
          echo "<input type='submit' name='artiste' value='$res'> <br>";
        }
        echo "</section>";
        echo "<section id='album'>";
        echo 'Album(s) :<br>';

        while ($row = mysqli_fetch_array($query2)) {
          $res = $row['titleAl'];
          echo "<input type='submit' name='album' value='$res'> <br>";
        }
        echo "</section>";
        echo "<section id='morceau'>";
        echo 'Morceau(x) <br>';
        while ($row = mysqli_fetch_array($query3)) {
          $res_morceau = $row['titleTr'];
          $res_morceau_album = $row['titleAl'];
          echo "<input type='submit' name='track' value='$res_morceau ($res_morceau_album)'> <br>";
        }
        echo "</section>";
        ?>
     </form>
     <?php
     echo '</section>';
     echo '<div id="droite"></div>';
      }
    }
    ?>
    <div id="pushFooter">
  </div>
    <footer>
      <section>

        <h2>Information</h2>
        <p>Site créé par Sosthène Leroy, Erik Vienne et Thomas Quetier dans le cadre d'un projet d'étude</p>
        <p>Pour tout contact = <a href="mailto:sosthene.leroy@etu.univ-orleans.fr">sosthene.leroy@etu.univ-orleans.fr</a></p>

      </section>

      <img src="../resources/logoIUT.png" alt="IUT orleans" height="100" width="150">
</footer>
</body>
</html>
